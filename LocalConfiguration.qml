import QtQuick 2.0
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import "./custom"

Item {
    id: base

    signal clicked
    property bool isActive

    // alias does not work for unknown reason
    onIsActiveChanged: { buttonLocalGame.isActive = isActive }

    height: 40
    width:  buttonLocalGame.width

    Button {
        id: buttonLocalGame
        text: "Start local game"

        onClicked: base.clicked()
    }

    Rectangle {
        anchors.left: buttonLocalGame.right
        anchors.verticalCenter: buttonLocalGame.verticalCenter
        anchors.margins: 10
        visible: isActive

        width:  labelInfo.width + 10
        height: labelInfo.height + 10

        Text {
            id: labelInfo
            anchors.centerIn: parent
            text: "Please add at least 2 players"
            color: "#b825d5"
            font.pointSize: 16
        }
    }
}
