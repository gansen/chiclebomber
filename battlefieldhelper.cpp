#include "battlefieldhelper.h"

#include "pyrotechnics.h"
#include "battlefield.h"
#include "avatar.h"
#include "tile.h"
#include "armoury.h"

#include <QDateTime>
#include <QVector>
#include <QDebug>

#define BF BF_instance
#define EXPOSE_BF \
    QVector<Tile> & tiles = BF.getField(); \
    uint columns = BF.columns(); \
    uint rows    = BF.rows(); \
    Q_UNUSED( columns ) Q_UNUSED( rows ) Q_UNUSED(tiles) // prevent warnings


namespace BattlefieldHelper
{

// Corners should be free from stones to prevent players
// from getting trapped before the game has even started
void freeCorners() {
    EXPOSE_BF;
    // left upper corner
    tiles[     columns + 1 ].setType( Tile::gangway );
    tiles[     columns + 2 ].setType( Tile::gangway );
    tiles[ 2 * columns + 1 ].setType( Tile::gangway );

    // right upper corner
    tiles[ 2 * columns - 2 ].setType( Tile::gangway );
    tiles[ 2 * columns - 3 ].setType( Tile::gangway );
    tiles[ 3 * columns - 2 ].setType( Tile::gangway );

    // right lower corner
    int lastPosition = (columns ) * (rows - 1) - 2;
    tiles[ lastPosition           ].setType( Tile::gangway );
    tiles[ lastPosition - 1       ].setType( Tile::gangway );
    tiles[ lastPosition - columns ].setType( Tile::gangway );

    // left lower corner
    tiles[ lastPosition - 2 * columns + 3 ].setType( Tile::gangway );
    tiles[ lastPosition -     columns + 3 ].setType( Tile::gangway );
    tiles[ lastPosition -     columns + 4 ].setType( Tile::gangway );
}

// Stones, those can be blown up, are filled in randomly.
void generateStones() {
    EXPOSE_BF;
    qsrand(QDateTime::currentDateTime().toTime_t());

    // Tile after the first starting corner
    int firstTile    = 3 + columns;
    // Tile before the last starting corner
    int lastPosition = columns * rows - firstTile - 1;

    for( int i = 0; i < lastPosition * 0.3; i++ ) {
        int index = firstTile + qrand() % lastPosition;
        if( tiles[ index ].type() != Tile::gangway )
            i--; // wall in the way? try again
        else {
            tiles[ index ].setType( Tile::stone );
            switch( qrand() % 5) {
            case 1:
                tiles[ index ].setPostExplosionType( Tile::pickupRangeExtension );
                break;
            case 2:
                tiles[ index ].setPostExplosionType( Tile::pickupBomb );
                break;
            default:
                ;
            }
        }
    }

    freeCorners();
}

// Generate walls in the typical CTwins pattern.
// 1. The border consists of walls
// 2. Not including border: First columns and first row is gangway
// 3. Each other line/column becomes a wall
// space - gangway, W - wall:
// wwwwwwwww
// w       W
// w w w w w
// w       w
// w w w w w
// w       W
// wwwwwwwww
void generateWalls() {
    EXPOSE_BF;

#if 1
    // populate 1st + last row
    for( uint c = 0; c < columns - 1; c++ ) {
        tiles[ c ].setType( Tile::wall );
        tiles[ c + 1 + (rows - 1) * columns ].setType( Tile::wall );
    }
#endif

#if 1
    // populate side Columns
    for( uint r = columns - 1; r <= (columns -1) * rows; r += columns ) {
        tiles[ r ].setType( Tile::wall );
        tiles[ r + 1 ].setType( Tile::wall );
    }
#endif

#if 1
    // populate center
    for( uint c = 2; c < columns - 2; c += 2 )
        for( uint r = 2; r < rows - 2; r += 2 ) {
            uint pos = columns * r + c;
            tiles[ pos ].setType( Tile::wall );
        }
#endif
}

void initializeStartPosition(Avatar * avatar) {
    EXPOSE_BF;
    Q_UNUSED(tiles)

    switch( avatar->id() ) {
    case 0:
        // left top corner
        avatar->setPositionX( BF.internalTileDimension() );
        avatar->setPositionY( BF.internalTileDimension() );
        avatar->setViewingDirection( ViewingDirection::Right );
        break;

    case 1:
        // right bottom corner
        avatar->setPositionX( (columns - 2) * BF.internalTileDimension() );
        avatar->setPositionY( (rows - 2)    * BF.internalTileDimension() );
        avatar->setViewingDirection( ViewingDirection::Left );
        break;

    case 2:
        // left bottom corner
        avatar->setPositionX( BF.internalTileDimension() );
        avatar->setPositionY( (rows - 2) * BF.internalTileDimension() );
        avatar->setViewingDirection( ViewingDirection::Right );
        break;

    case 3:
        // right top corner
        avatar->setPositionX( (columns - 2) * BF.internalTileDimension() );
        avatar->setPositionY( BF.internalTileDimension() );
        avatar->setViewingDirection( ViewingDirection::Left );
        break;
    }
}

int tilesToLinear( Row r, Column c )
{
    EXPOSE_BF;
    return r * columns + c;
}

void connectTiles() {
    EXPOSE_BF;

    for( uint c = 1; c < columns; c++ )
        for( uint r = 1; r < rows - 1; r++ ) {
            Tile * t  = & tiles[ tilesToLinear( r, c ) ];

            t->left   = & tiles[ tilesToLinear( r,     c - 1 ) ];
            t->right  = & tiles[ tilesToLinear( r,     c + 1 ) ];
            t->up     = & tiles[ tilesToLinear( r - 1, c     ) ];
            t->down   = & tiles[ tilesToLinear( r + 1, c     ) ];
        }

    // fix corner cases
    // should not be necessary since these are walls
    for( uint r = 0; r < rows; r++ ) {
        Tile * tl = & tiles[ tilesToLinear( r,           0 ) ];
        Tile * tr = & tiles[ tilesToLinear( r, columns - 1 ) ];

        tl->left  = nullptr;
        tr->right = nullptr;
    }

    for( uint c = 0; c < columns; c++ ) {
        Tile * tu = & tiles[ tilesToLinear( 0,        c ) ];
        Tile * td = & tiles[ tilesToLinear( rows - 1, c ) ];

        tu->up   = nullptr;
        td->down = nullptr;
    }

    Tile * tlu = & tiles[ tilesToLinear( 0,        0 ) ];
    tlu->left  = nullptr;
    tlu->up    = nullptr;

    Tile * tlb = & tiles[ tilesToLinear( rows - 1, 0 ) ];
    tlb->left  = nullptr;
    tlb->down  = nullptr;

    Tile * tru = & tiles[ tilesToLinear( 0,        columns - 1 ) ];
    tru->right = nullptr;
    tru->up    = nullptr;

    Tile * trb = & tiles[ tilesToLinear( rows - 1, columns - 1 ) ];
    trb->right = nullptr;
    trb->down  = nullptr;

    for( int i = 0; i < tiles.size(); i++ )
        tiles[ i ].linearCoordinate = i;
}

void initialize() {
    EXPOSE_BF;

    tiles.clear();
    tiles.insert( 0, columns * rows, Tile() );
    connectTiles();
    generateWalls();
    generateStones();
}

int unifiedCoordinatesToLinearTile(int x, int y)
{
    EXPOSE_BF;

    Column column = x / BF.internalTileDimension();
    Row    row    = y / BF.internalTileDimension();


    return row * columns + column;
}

Drift clampToRasterXright(int x, int y, int & newX )
{
    EXPOSE_BF;
    // Check if the right side crosses a tile boundary
    Column c1 = ( newX + BF.internalTileDimension() - 1 ) / BF.internalTileDimension();
    Column c2 = ( x    + BF.internalTileDimension() - 1 ) / BF.internalTileDimension();
    if( c1 == c2 ) // Nope? then return
        return Drift::OK;

    // Check for obstacles in the right upper coordinates
    Tile * t = getTile( newX + BF.internalTileDimension() - 1, y );

    if( t->isObstacle() ) // then go the maximum to the right
        newX = (newX / BF.internalTileDimension()) * BF.internalTileDimension();

    // ...in the right lower coordinates
    t = getTile( newX + (BF.internalTileDimension() - 1), y + (BF.internalTileDimension() - 1) );

    if( t->isObstacle() )
        newX = (newX / BF.internalTileDimension()) * BF.internalTileDimension();

    return Drift::OK;
}



Drift clampToRasterXleft(int x, int y, int & newX)
{
    EXPOSE_BF;
    // Check if the left side crosses a tile boundary
    Column c1 = newX / BF.internalTileDimension();
    Column c2 = x    / BF.internalTileDimension();
    if( c1 == c2 ) // Nope? then return
        return Drift::OK;

    Tile * t = getTile( newX, y );

    // Check for obstacles in the left upper coordinates
    if( t->isObstacle() ) // then don't move (that's why we take x not newX)
        newX = (x / BF.internalTileDimension()) * BF.internalTileDimension();

    // ...in the left lower coordinates
    t = getTile( newX, y + (BF.internalTileDimension() - 1) );

    if( t->isObstacle() )
        newX = (x / BF.internalTileDimension()) * BF.internalTileDimension();

    return Drift::OK;
}



Drift clampToRasterYup(int x, int y, int & newY)
{
    EXPOSE_BF;
    // Check if top crosses tile boundary
    Row r1 = newY / BF.internalTileDimension();
    Row r2 = y    / BF.internalTileDimension();
    if( r1 == r2 ) // Nope? then return
        return Drift::OK;

    // Check for obstacles in the left upper coordinates
    Tile * t = getTile( x, newY );

    if( t->isObstacle() ) // then don't move (that's why we take y, not newY)
        newY = (y / BF.internalTileDimension()) * BF.internalTileDimension();

    // ...in the right upper coordinates
    t = getTile( x + (BF.internalTileDimension() - 1), newY );

    if( t->isObstacle() )
        newY = (y / BF.internalTileDimension()) * BF.internalTileDimension();

    return Drift::OK;
}

Drift clampToRasterYdown(int x, int y, int & newY)
{
    EXPOSE_BF;
    // Check if bottom crosses tile boundary
    Row r1 = ( newY + BF.internalTileDimension() - 1 ) / BF.internalTileDimension();
    Row r2 = ( y    + BF.internalTileDimension() - 1 ) / BF.internalTileDimension();
    if( r1 == r2 ) // Nope? then return
        return Drift::OK;

    // Check for obstacles in the left lower coordinates
    Tile * t = getTile( x, newY + BF.internalTileDimension() );

    if( t->isObstacle() ) // then don't move
        newY = (newY / BF.internalTileDimension()) * BF.internalTileDimension();

    // ...in the right lower coordinates
    t = getTile( x + (BF.internalTileDimension() - 1), newY + (BF.internalTileDimension() - 1) );

    if( t->isObstacle() )
        newY = (newY / BF.internalTileDimension()) * BF.internalTileDimension();

    return Drift::OK;
}

Tile *getTile(int x, int y)
{
    EXPOSE_BF;
    return & tiles[ unifiedCoordinatesToLinearTile( x, y ) ];
}

void placeBomb( Avatar * avatar )
{
    // Bombs are placed in the "middle" of an avatar, get the coordinates:
    Tile * t =  getCenteredTile( avatar->positionX(), avatar->positionY() );

    if( t->isObstacle() ) return;

    PY_instance.requestExplosion( avatar->armoury(), t );
}

Tile *getCenteredTile(int x, int y)
{
    x += BF.internalTileDimension() / 2;
    y += BF.internalTileDimension() / 2;

    return BF_instance.getTile( unifiedCoordinatesToLinearTile( x, y ) );
}


}
