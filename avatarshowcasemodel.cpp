#include "avatarshowcasemodel.h"

#include "playerinputconfig.h"
#include "gamecoordinator.h"
#include "genericmacros.h"
#include "avatar.h"

#include <QVector>

#define MAX_AVATARS 4

class AvatarShowCaseModel::AvatarShowCaseModelPrivate
{
public:
    int playerNumber = 0;
};

AvatarShowCaseModel::AvatarShowCaseModel(QObject *parent) : QAbstractListModel(parent)
      , d(new AvatarShowCaseModelPrivate)
{
    connect( GC_instance, SIGNAL(avatarsUpdated()), SLOT(avatarsUpdated()) );
}

AvatarShowCaseModel::~AvatarShowCaseModel() { delete d; }

void AvatarShowCaseModel::createAvatar()
{
    int s = GC_instance->avatars().size();
    beginInsertRows(QModelIndex(), s, s );
    GC_instance->registerPlayer( true );
    endInsertRows();

    setPlayerNumber( GC_instance->avatars().size() );
}

void AvatarShowCaseModel::removeAvatar(int id)
{
    beginResetModel();

    GC_instance->unRegisterPlayer( id );
    setPlayerNumber( GC_instance->avatars().size() );

    endResetModel();
}


int AvatarShowCaseModel::rowCount(const QModelIndex &index) const
{
    Q_UNUSED(index)
    return GC_instance->avatars().size();
}

QVariant AvatarShowCaseModel::data(const QModelIndex & index, int role) const
{
    QVector<Avatar *> avatars( GC_instance->avatars() );
    int row = index.row();

    Avatar * avatar = avatars[ row ];


    switch( role ) {
    case Roles::imagePath:
        return avatar->sideImage();
    case Roles::localPlayer:
        return QVariant( avatar->isLocal() );
    case Roles::avatarId:
        return QVariant( avatar->id() );
    case Roles::inputConfig:
        return QVariant::fromValue( &avatar->playerInputConfig() );

    }
    return QVariant();
}

SGETTER_PROPERTY( AvatarShowCaseModel, setPlayerNumber, int, playerNumber )

QHash<int, QByteArray> AvatarShowCaseModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[ Roles::imagePath ]   = "imagePath";
    roles[ Roles::localPlayer ] = "localPlayer";
    roles[ Roles::avatarId ]    = "avatarId";
    roles[ Roles::inputConfig ] = "inputConfig";

    return roles;
}

void AvatarShowCaseModel::avatarsUpdated()
{
    beginResetModel();
    endResetModel();
}
