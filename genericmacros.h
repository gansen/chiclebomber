#ifndef GENERICMACROS_H
#define GENERICMACROS_H


// macros for Qt properties
#define SETTER_PROPERTY( className, setterName, type, variableName ) \
void className::setterName( type variableName ) { \
    d->variableName = variableName; \
    emit variableName##Changed(); \
}

#define GETTER_PROPERTY( className, type, variableName ) \
type className::variableName() { \
    return d->variableName; \
}

#define SGETTER_PROPERTY( className, setterName, type, variableName ) \
SETTER_PROPERTY( className, setterName, type, variableName ) \
GETTER_PROPERTY( className, type, variableName ) \




// generic setter getter macros
#define GETTER( className, type, variableName ) \
type className::variableName() { \
    return d->variableName; \
}


#define SETTER( className, setterName, type, variableName ) \
void className::setterName( type variableName ) { \
    d->variableName = variableName; \
}

#define SETTER_SIGNAL( className, setterName, type, variableName, signalName ) \
void className::setterName( type variableName ) { \
    d->variableName = variableName; \
    emit signalName(); \
}


#define SGETTER( className, setterName, type, variableName ) \
SETTER( className, setterName, type, variableName ) \
GETTER( className, type, variableName ) \

#endif // GENERICMACROS_H
