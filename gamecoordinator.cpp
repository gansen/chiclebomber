#include "gamecoordinator.h"

#include "playerinputhandler.h"
#include "playerinputhelper.h"
#include "battlefield.h"
#include "genericmacros.h"
#include "avatar.h"

#include <QTimer>
#include <QSet>

const int keyboardSampleRate = 30;

class GameCoordinator::GameCoordinatorPrivate
{
public:
    GameCoordinatorPrivate( GameCoordinator * gc ) :
          q( gc )
    {
        connect( &timer, SIGNAL( timeout() ), q, SLOT( tick() ) );
    }

    void start() {

        BF_instance.initialize();

        for( Avatar * a : avatars )
            a->initialize();

        pih.start();
        timer.start( keyboardSampleRate );
    }

    void stop() {
        pih.stop();
        timer.stop();
    }

    void unRegisterPlayer( int id ) {
        Avatar * avatar = nullptr;
        for( Avatar * a : avatars ) {
            if( a->id() == id ) {
                avatar = a;
                avatars.removeOne( a );
                break;
            }
        }

        // We delete the avatar later, since qml
        // still accesses a pointer to pih
        avatar->deleteLater();

        usedAvatarIds.remove( id );
    }

    uint getFreeAvatarId() {
        uint i = 0;
        for( i = 0; i < 4; i++ )
            if( not usedAvatarIds.contains( i ) ) break;

        usedAvatarIds.insert( i );
        return i;
    }

    void registerPlayer( bool isLocal ) {
        Avatar * avatar = new Avatar;
        uint id = getFreeAvatarId();

        connect( avatar, SIGNAL(outOfLifes(Avatar*)), q, SLOT(playerDied(Avatar*)) );
        avatar->setIsLocal( isLocal );
        avatar->setId( id );
        avatar->setSideImage(  QString("images/avatar_side_%1.png").arg(  id + 1 ));
        avatar->setFrontImage( QString("images/avatar_front_%1.png").arg( id + 1 ));
        avatar->setBackImage( QString("images/avatar_back_%1.png").arg(   id + 1 ));

        PlayerInputHelper::configureStandardInput( avatar );

        avatars.append(avatar);
    }

    ~GameCoordinatorPrivate() {
        qDeleteAll( avatars );
        avatars.clear();

        qDeleteAll( diedAvatars );
        diedAvatars.clear();
    }

    int port;
    PlayerInputHandler pih;
    GameCoordinator * q;
    QTimer timer;
    QVector<Avatar *> avatars;
    QVector<Avatar *> diedAvatars;
    QSet< uint > usedAvatarIds;
};


GameCoordinator::GameCoordinator(QObject *parent) : QObject(parent)
      , d( new GameCoordinatorPrivate(this) )
{
}

GameCoordinator::~GameCoordinator() { delete d; }

void GameCoordinator::stop()  { d->stop(); }

void GameCoordinator::start() {
    d->start();
    emit gameStarted();
}

QVector<Avatar *> & GameCoordinator::avatars() { return d->avatars; }

int GameCoordinator::numberPlayers()  { return d->avatars.size(); }

SGETTER_PROPERTY( GameCoordinator, setPort, int, port )

Avatar * GameCoordinator::registerPlayer( bool isLocal )
{
    d->registerPlayer( isLocal );
    return d->avatars.last();
}

void GameCoordinator::unRegisterPlayer(int id) { d->unRegisterPlayer( id ); }

void GameCoordinator::playerDied(Avatar *avatar)
{
    d->avatars.removeOne( avatar );
    disconnect( avatar, SIGNAL(outOfLifes(Avatar*)) );
    d->diedAvatars.append( avatar );

    if( d->avatars.size() == 1 ) {
        emit gameOver();

        disconnect( d->avatars.first(), SIGNAL(outOfLifes(Avatar*)) );

        //Revive dead avatars, show be shown on final screen
        while( d->diedAvatars.size() ) {
            Avatar * a = d->diedAvatars.takeLast();
            d->avatars.append( a );
        }
    }

    emit avatarsUpdated();
}



void GameCoordinator::tick()
{
    emit newFrame();
}


