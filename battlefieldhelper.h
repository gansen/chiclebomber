#ifndef BATTLEFIELDHELPER_H
#define BATTLEFIELDHELPER_H

#include "types.h"

#include <QVector>

class Avatar;
class Tile;

namespace BattlefieldHelper
{

void initialize();

void initializeStartPosition( Avatar * avatar );

int tilesToLinear( Row r, Column column );

Tile * getTile( int x, int y );
Tile * getCenteredTile( int x, int y );

int unifiedCoordinatesToLinearTile( int x, int y );


// Checks for collision and corrects x ( resp. y ) if necessary.
// If the player is on an edge, the function returns a Drift hint,
// helping to around the edge.
Drift clampToRasterXleft ( int x,  int y, int & newX );
Drift clampToRasterXright( int x,  int y, int & newX );

Drift clampToRasterYup  ( int x,  int y, int & newY );
Drift clampToRasterYdown( int x,  int y, int & newY );

void  placeBomb( Avatar * avatar );

}

#endif // BATTLEFIELDHELPER_H
