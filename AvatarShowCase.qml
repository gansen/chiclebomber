import QtQuick 2.0
import AvatarShowCaseModel 1.0

ListView {
    id: base
    orientation: ListView.Horizontal
    interactive: false

    property bool  readyToStart : avatarModel.playerNumber >= 2
    property int   avatarViewWidth : base.height

    model: AvatarShowCaseModel {
        id: avatarModel
    }

    delegate: Item {

        height: avatarViewWidth
        width:  avatarViewWidth

        AvatarView {
            id: avatarView
            anchors.centerIn: parent

            scale: avatarViewWidth / 100.0
            border.width: 1
            radius: 10
            imageSource: imagePath
            avatarIdentifier: avatarId

            playerInputConfig: inputConfig


            onUnRegister: avatarModel.removeAvatar( id )
        }
    }

    ButtonAvatarAdd {
        visible: avatarModel.playerNumber < 4
        height: avatarViewWidth
        width:  avatarViewWidth
        x:  avatarViewWidth * avatarModel.playerNumber
        onClicked: avatarModel.createAvatar()
    }
}
