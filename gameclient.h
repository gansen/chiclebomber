#ifndef GAMECLIENT_H
#define GAMECLIENT_H

#include <QObject>

class QQmlEngine;
class QJSEngine;

class GameClient : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(GameClient)
    Q_PROPERTY(bool isConnected READ isConnected NOTIFY isConnectedChanged)
public:

    static QObject * instance( QQmlEngine *, QJSEngine * ) {
        static GameClient * gc = 0;
        if( not gc ) gc = new GameClient;
        return gc;
    }

    Q_INVOKABLE bool connect( QString host, quint16 port );
    Q_INVOKABLE void disconnect();

    bool isConnected();

signals:
    void isConnectedChanged();

public slots:

private:
    explicit GameClient(QObject *parent = 0);
    ~GameClient();

    class GameClientPrivate;
    GameClientPrivate * d;
};

#endif // GAMECLIENT_H
