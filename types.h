#ifndef TYPES_H
#define TYPES_H

#include <QPair>

typedef unsigned int KeyCode;
typedef unsigned int Column;
typedef unsigned int Row;

typedef unsigned int         LinearCoordinate;

enum class Drift {
    OK = 0,
    TOP,
    BOTTOM,
    LEFT,
    RIGHT,
    BLOCK
};


enum class ViewingDirection {
    Left = 0,
    Right,
    Up,
    Down,
};


#endif // TYPES_H
