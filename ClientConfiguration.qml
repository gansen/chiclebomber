import QtQuick 2.0
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import "./custom"

ColumnLayout {
    id: base

    signal clicked
    property alias ipAddress : ipAddress.text
    property alias port      : port.text
    property bool isActive

    // alias does not work for unknown reason
    onIsActiveChanged: { buttonBeClient.isActive = isActive }

    height: 100
    width:  buttonBeClient.width

    Item {
        anchors.fill: parent

        Button {
            id: buttonBeClient
            text: "Start as Client"

            onClicked: base.clicked()
        }


        ColumnLayout {
            anchors.top: buttonBeClient.bottom
            anchors.horizontalCenter: buttonBeClient.horizontalCenter
            anchors.margins: 10
            visible: isClientMode

            RowLayout {
                Label {
                    id: labelClientIP
                    font.pixelSize: 16
                    text: "IP:"
                }

                TextInput {
                    id: ipAddress
                    width: 200
                    text: "some ip"
                    font.pixelSize: 20
                }
            }

            RowLayout {
                Label {
                    id: labelClientPort
                    font.pixelSize: 16
                    text: "Port:"
                }

                TextInput {
                    id: port
                    width: 200
                    text: "64000"
                    font.pixelSize: 20
                }
            }
        }
    }
}
