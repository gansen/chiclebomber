import QtQuick 2.7
import QtGraphicalEffects 1.0
import PlayerInputConfig 1.0
import QtQuick.Layouts 1.1
import Avatar 1.0


Rectangle {
    id: base
    color: "aliceblue"
    width:  100
    height: 100

    signal unRegister( int id )

    property alias             imageSource : avatarImage.source
    property int               avatarIdentifier
    property PlayerInputConfig playerInputConfig

    ColumnLayout {
        anchors.fill: parent

        RowLayout {
            id: itemAvatar
            Layout.fillHeight: true
            Layout.fillWidth:  true
            Layout.topMargin: 5
            Layout.preferredHeight: 40

            Item {
                Layout.fillHeight: true
                Layout.fillWidth:  true
                Layout.preferredHeight: 20
                Layout.preferredWidth:  20

                Image {
                    id: avatarImage
                    anchors.centerIn: parent

                    width:  ( parent.width < parent.height ? parent.width : parent.height ) - 2
                    height: ( parent.width < parent.height ? parent.width : parent.height ) - 2
                }
            }


            Item {
                id: substractImage
                Layout.fillHeight: true
                Layout.fillWidth:  true
                Layout.preferredHeight: 20
                Layout.preferredWidth:  20

                Image{
                    width:  ( parent.width < parent.height ? parent.width : parent.height ) - 4
                    height: ( parent.width < parent.height ? parent.width : parent.height ) - 4
                    anchors.centerIn: parent
                    source: "images/delete_X.png"
                    opacity: 0.7

                    MouseArea {
                        anchors.fill: parent
                        hoverEnabled: true

                        onEntered: {
                            substractImage.scale = 1.2
                            substractImage.opacity = 1
                            avatarImage.scale    = 0.8
                        }

                        onExited: {
                            substractImage.scale = 1
                            substractImage.opacity = 0.7
                            avatarImage.scale    = 1
                        }

                        onClicked: base.unRegister( base.avatarIdentifier )
                    }
                }
            }
        }

        Rectangle {
            color: "red"
            Layout.preferredHeight: 10
            Layout.topMargin:    2
            Layout.bottomMargin: 2
            Layout.leftMargin:  10
            Layout.rightMargin: 10

            Text {
                id: dummyText
                text: "Your input choice"
                font.pixelSize: 10
            }
        }


        Item {
            id: statusLine
            Layout.preferredHeight: 40
            Layout.fillWidth: true
            Layout.bottomMargin: 5
            Layout.leftMargin:  10
            Layout.rightMargin: 10

            KeyboardConfigView {
                anchors { top: parent.top; bottom: parent.bottom; left: parent.left; right: parent.right }
                onMouseHoveringStart: itemAvatar.opacity = 0.7
                onMouseHoveringEnd:   itemAvatar.opacity = 1.0
                pic: playerInputConfig
            }
        }
    }
}
