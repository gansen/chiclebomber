#include "pyrotechnics.h"

#include "tile.h"
#include "avatar.h"
#include "armoury.h"
#include "explosion.h"
#include "battlefield.h"
#include "gamecoordinator.h"
#include "battlefieldhelper.h"

#include <QVector>
#include <QDebug>
#include <QTimer>
#include <QTime>

const int igniteTime = 2000; // msec

class Pyrotechnics::PyrotechnicsPrivate
{
public:
    PyrotechnicsPrivate() { stopwatch.start(); }

    bool requestExplosion( Armoury & armoury, Tile * t ) {

        if( not armoury.dropBomb() )
            return false;

        Explosion * expl = new Explosion;

        expl->tile        =   t;
        expl->startTime   =   stopwatch.elapsed();
        expl->armoury     = & armoury;
        // We copy the range so that in case it gets leveld up
        // placed bombs will still use the range that it was placed with.
        expl->range       =   armoury.bombRange();

        if( not timer.isActive() )
            timer.start( igniteTime );

        t->setType( Tile::bomb );

        explosions.append( expl );

        return true;
    }

    bool analyseTile( QVector< Tile * > & involvedTiles,
                     QVector< Tile * > & ignitedBombs,
                     Tile * t ) {

        switch( t->type() ) {

        case Tile::bomb:
            ignitedBombs.append( t );
            t->setType( Tile::gangway ); // prevent from finding the bomb twice
        case Tile::gangway:
        case Tile::pickupRangeExtension:
        case Tile::pickupBomb:
        case Tile::explosion:
            involvedTiles.append( t );
            return false;

        case Tile::stone:
            involvedTiles.append( t );
        case Tile::wall:
        default:
            return true;
            ;
        }

    }

    void checkForKill( Explosion * expl ) {
        for( Tile * t : expl->involvedTiles ) {
            for( Avatar * a : GC_instance->avatars() )
                if( t == a->tile() )
                    a->loseLife();
        }
    }

    void calculateOneDetonation( Explosion * expl,
                                 QVector< Tile * > & involvedTiles,
                                 QVector< Tile * > & chainIgnitedBombs ) {

        uint range          = expl->range;
        Tile * tileOfOrigin = expl->tile;

        Tile * t = tileOfOrigin;
        for( uint i = 1; i <= range; i++ ) {

            t = t->right;

            if( analyseTile( involvedTiles, chainIgnitedBombs, t ) )
                break;
        }

        t = tileOfOrigin;
        for( uint i = 1; i <= range; i++ ) {

            t = t->left;

            if( analyseTile( involvedTiles, chainIgnitedBombs, t ) )
                break;
        }

        t = tileOfOrigin;
        for( uint i = 1; i <= range; i++ ) {

            t = t->up;

            if( analyseTile( involvedTiles, chainIgnitedBombs, t ) )
                break;
        }

        t = tileOfOrigin;
        for( uint i = 1; i <= range; i++ ) {

            t = t->down;

            if( analyseTile( involvedTiles, chainIgnitedBombs, t ) )
                break;
        }
    }

    Explosion * lookupExplosion( Tile * t ) {
        for( Explosion * e : explosions )
            if( e->tile == t ) {
                explosions.removeOne( e );
                return e;
            }

        return nullptr; // can't happen
    }

    void boom() {
        Explosion * srcExpl = explosions.takeFirst();

        currentlyExploding.append( srcExpl );

        srcExpl->involvedTiles.append( srcExpl->tile );

        // reset tile so no bomb is found in subsequent routines
        srcExpl->tile->setType( Tile::gangway );

        QVector< Tile * > chainIgnitedBombs;

        calculateOneDetonation( srcExpl, srcExpl->involvedTiles, chainIgnitedBombs );
        srcExpl->armoury->returnBombAfterExplosion();

        while( not chainIgnitedBombs.isEmpty() ) {

            Explosion * expl = lookupExplosion( chainIgnitedBombs.takeFirst() );

            calculateOneDetonation( expl, srcExpl->involvedTiles, chainIgnitedBombs );
            expl->armoury->returnBombAfterExplosion();
            delete expl;
        };

        // check kills
        checkForKill( srcExpl );

        for( Tile * t : srcExpl->involvedTiles )
            t->explosionStart();

        if( explosions.isEmpty() )
            return;

        //Set the timer for the next explosinon
        //and make sure it is not negative
        timer.start( std::max( 0, igniteTime - (stopwatch.elapsed() - explosions.at( 0 )->startTime) ) );
    }


    QVector< Explosion * > explosions;
    QVector< Explosion * > currentlyExploding;
    QTimer timer;
    QTime  stopwatch;
};


Pyrotechnics::Pyrotechnics(QObject *parent) : QObject(parent)
      , d(new PyrotechnicsPrivate)
{
    connect( & d->timer,    SIGNAL(timeout()),  SLOT(boom()) );
    connect(   GC_instance, SIGNAL(newFrame()), SLOT(newFrame()) );

    d->timer.setSingleShot( true );
}

Pyrotechnics::~Pyrotechnics() { delete d; }

bool Pyrotechnics::requestExplosion(Armoury &armoury, Tile *t) {
    return d->requestExplosion( armoury, t ); }

void Pyrotechnics::boom() {
    d->boom();
}

void Pyrotechnics::newFrame()
{
    for( Explosion * e : d->currentlyExploding ) {
        e->animationTimer--;
        d->checkForKill( e );
        if( e->animationTimer < 0 ) {
            for( Tile * t : e->involvedTiles )
                t->explosionStop();
            d->currentlyExploding.removeOne( e );
        }
    }

}
