import QtQuick 2.7
import QtGraphicalEffects 1.0

Rectangle {
    id: base
    border.width: 1
    color: "aliceblue"
    radius: 20

    signal clicked

    Image {
        anchors.centerIn: parent
        source: "images/add_idle.png"
    }

        RadialGradient {
            anchors.fill: parent
            anchors.margins: 25
            gradient: Gradient {
                GradientStop {
                    position: 0.0;
                    SequentialAnimation on color {
                        id: animation
                        loops: Animation.Infinite
                        alwaysRunToEnd: true
                        running: false
                        ColorAnimation { from: "#000000"; to: "red"; duration: 200 }
                        ColorAnimation { from: "red"; to: "blue"; duration: 200 }
                        ColorAnimation { from: "blue"; to: "#000000"; duration: 200 }
                    }
                }
                GradientStop { position: 0.5; color: "transparent" }
            }
        }

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        onEntered:  animation.restart()
        onExited:   animation.stop()
        onClicked:  base.clicked()
    }
}
