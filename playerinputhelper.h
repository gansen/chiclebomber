#ifndef PLAYERINPUTHELPER_H
#define PLAYERINPUTHELPER_H

#include "types.h"

class Avatar;


namespace PlayerInputHelper
{
    void configureStandardInput( Avatar * avatar );

    void applyMovement( Avatar * avatar, KeyCode keyCode );
}

#endif // PLAYERINPUTHELPER_H
