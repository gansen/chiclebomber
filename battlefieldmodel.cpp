#include "battlefieldmodel.h"

#include "genericmacros.h"
#include "battlefield.h"
#include "tile.h"



class BattlefieldModel::BattlefieldModelPrivate
{
public:
    BattlefieldModelPrivate( BattlefieldModel * q ) :
            q(q) {}

    void initialize() {
        int rasterSize = rows * columns;
        if( rasterSize == 0 )
            return;

        q->beginInsertRows( QModelIndex(), 0, rasterSize );
        BF_instance.setColumns( columns );
        BF_instance.setRows( rows );
        q->endInsertRows();
    }

    QString wallImage, stoneImage, bombImage, explosionImage, rangeExtension;
    QString pickupBomb;
    BattlefieldModel * q;
    uint columns = 0, rows = 0;
};

BattlefieldModel::BattlefieldModel() :
        d( new BattlefieldModelPrivate( this ) )
{
    connect( &BF_instance, SIGNAL(tilesChanged(QSet<LinearCoordinate>)),
             this, SLOT(refresh(QSet<LinearCoordinate>)));

    connect( &BF_instance, SIGNAL(rasterInitialized()), SLOT(rasterReset()) );
}

BattlefieldModel::~BattlefieldModel() { delete d; }

int BattlefieldModel::columns() { return BF_instance.columns(); }

void BattlefieldModel::setColumns(int columns) {
    d->columns = columns;
    d->initialize();
}

int BattlefieldModel::rows() { return BF_instance.rows(); }

void BattlefieldModel::setRows(int rows) {
    d->rows = rows;
    d->initialize();
}


SGETTER( BattlefieldModel, setWallImage,       QString, wallImage )
SGETTER( BattlefieldModel, setStoneImage,      QString, stoneImage )
SGETTER( BattlefieldModel, setBombImage,       QString, bombImage )
SGETTER( BattlefieldModel, setExplosionImage,  QString, explosionImage )
SGETTER( BattlefieldModel, setRangeExtension,  QString, rangeExtension )
SGETTER( BattlefieldModel, setPickupBomb,      QString, pickupBomb )


int BattlefieldModel::rowCount(const QModelIndex &parent) const
{
    if(parent.isValid()) return 0;
    return BF_instance.rasterSize();
}


QVariant BattlefieldModel::data(const QModelIndex &index, int role ) const
{
    Tile * tile = BF_instance.getTile( index.row() );
    if( role == Roles::animated ) {
        if( tile->type() == Tile::pickupBomb )
            return QVariant(d->pickupBomb);
        else
            return QVariant( "" );
    }

    switch( tile->type() ) {
    case Tile::wall:
        return QVariant(d->wallImage);
    case Tile::stone:
        return QVariant(d->stoneImage);
    case Tile::explosion:
        return QVariant(d->explosionImage);
    case Tile::pickupRangeExtension:
        return QVariant(d->rangeExtension);
    case Tile::bomb:
        return QVariant(d->bombImage);
    default:
        return QVariant("");
    }
}


QHash<int, QByteArray> BattlefieldModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[ Roles::content ]  = "content";
    roles[ Roles::animated ] = "animated";
    return roles;
}


void BattlefieldModel::refresh( const QSet< LinearCoordinate > & indices ) {
    for( LinearCoordinate lc : indices ) {
        QModelIndex a = index( lc );
        emit dataChanged( a, a );
    }
}

void BattlefieldModel::rasterReset() {
    beginResetModel();
    endResetModel();
}
