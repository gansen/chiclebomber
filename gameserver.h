#ifndef GAMESERVER_H
#define GAMESERVER_H

#include <QObject>

class QQmlEngine;
class QJSEngine;

class GameServer : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(GameServer)

public:

    static QObject * instance( QQmlEngine *, QJSEngine * ) {
        static GameServer * gs = 0;
        if( not gs ) gs = new GameServer;
        return gs;
    }

    Q_INVOKABLE void start( quint16 port );
    Q_INVOKABLE void stop();

signals:

public slots:
    //network
    void newIncommingConnection();
    void connectionDisconnected();

private:
    explicit GameServer(QObject *parent = 0);
    ~GameServer();
    class GameServerPrivate;
    GameServerPrivate * d;
};

#endif // GAMESERVER_H
