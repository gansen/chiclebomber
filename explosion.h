#ifndef EXPLOSION_H
#define EXPLOSION_H

#include <QObject>
#include <QVector>

class Armoury;
class Tile;

class Explosion : public QObject
{
    Q_OBJECT
public:
    explicit Explosion(QObject *parent = 0);

    Tile * tile;

    Armoury * armoury;

    int startTime;

    int range;

    QVector< Tile * > involvedTiles;

    int animationTimer = 50;

signals:

public slots:
};

#endif // EXPLOSION_H
