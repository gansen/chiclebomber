#ifndef BATTLEFIELDMODEL_H
#define BATTLEFIELDMODEL_H

#include "types.h"

#include <QAbstractListModel>
#include <QObject>
#include <QSet>

class Avatar;

class BattlefieldModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY( int columns READ columns WRITE setColumns )
    Q_PROPERTY( int rows    READ rows    WRITE setRows    )

    Q_PROPERTY( QString wallImage       READ wallImage       WRITE setWallImage  )
    Q_PROPERTY( QString stoneImage      READ stoneImage      WRITE setStoneImage )
    Q_PROPERTY( QString bombImage       READ bombImage       WRITE setBombImage )
    Q_PROPERTY( QString explosionImage  READ explosionImage  WRITE setExplosionImage )
    Q_PROPERTY( QString rangeExtension  READ rangeExtension  WRITE setRangeExtension )
    Q_PROPERTY( QString pickupBomb      READ pickupBomb      WRITE setPickupBomb )


public:
    BattlefieldModel();
    ~BattlefieldModel();

    enum Roles {
        content = Qt::UserRole,
        animated,
    };

    // Define the game raster
    int  columns();
    void setColumns( int columns );
    int  rows();
    void setRows( int rows );

    QString wallImage();
    void    setWallImage( QString image );

    QString stoneImage();
    void    setStoneImage( QString image );

    QString bombImage();
    void    setBombImage( QString image );

    QString explosionImage();
    void    setExplosionImage( QString image );

    QString rangeExtension();
    void    setRangeExtension( QString image );

    QString pickupBomb();
    void    setPickupBomb( QString image );


    // QAbstractItemModel interface
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole ) const;
    QHash<int, QByteArray> roleNames() const;

public slots:
    void refresh( const QSet< LinearCoordinate > & indices );

    void rasterReset();

private:
    class BattlefieldModelPrivate;
    BattlefieldModelPrivate * d;
};

#endif // BATTLEFIELDMODEL_H
