#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "playerinputconfig.h"
#include "avatarshowcasemodel.h"
#include "battlefieldmodel.h"
#include "gamecoordinator.h"
#include "avatarmodel.h"
#include "gameserver.h"
#include "gameclient.h"
#include "avatar.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    qmlRegisterType<AvatarShowCaseModel>( "AvatarShowCaseModel", 1, 0, "AvatarShowCaseModel" );
    qmlRegisterType<BattlefieldModel>("BattlefieldModel", 1, 0, "BattlefieldModel" );
    qmlRegisterType<AvatarModel>("AvatarModel", 1, 0, "AvatarModel" );
    qmlRegisterType<Avatar>("Avatar", 1, 0, "Avatar" );

    qmlRegisterSingletonType<GameCoordinator>(
                "GameCoordinator", 1, 0, "GameCoordinator", GameCoordinator::instance );

    qmlRegisterSingletonType<GameServer>(
                "GameServer", 1, 0, "GameServer", GameServer::instance );

    qmlRegisterSingletonType<GameClient>(
                "GameClient", 1, 0, "GameClient", GameClient::instance );

    qmlRegisterUncreatableType<PlayerInputConfig>(
                "PlayerInputConfig", 1, 0, "PlayerInputConfig", "Not creatable in Qml." );

    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    return app.exec();
}
