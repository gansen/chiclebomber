#ifndef GAMECOORDINATOR_H
#define GAMECOORDINATOR_H

#include <QObject>
#include <QVector>

class QQmlEngine;
class QJSEngine;
class Avatar;

#define GC_instance (static_cast<GameCoordinator *>(GameCoordinator::instance( 0, 0 )) )

class GameCoordinator : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(GameCoordinator)


public:

    static QObject * instance( QQmlEngine *, QJSEngine * ) {
        static GameCoordinator * gc = 0;
        if( not gc ) gc = new GameCoordinator;
        return gc;
    }

    Q_INVOKABLE void stop();
    Q_INVOKABLE void start();

    QVector<Avatar *> & avatars();
    int numberPlayers();

    int  port();
    void setPort( int port );

    Avatar * registerPlayer( bool isLocal );
    void     unRegisterPlayer( int id );


signals:
    void newFrame();
    void portChanged();

    void gameStarted();
    void gameOver();

    void avatarsUpdated();

public slots:

    void playerDied( Avatar * avatar );

private slots:
    void tick();

private:
    explicit GameCoordinator(QObject *parent = 0);
    ~GameCoordinator();
    class GameCoordinatorPrivate;
    GameCoordinatorPrivate * d;
};

#endif // GAMECOORDINATOR_H
