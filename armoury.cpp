#include "armoury.h"

class Armoury::ArmouryPrivate
{
public:

    void initialize() {
        bombsOnField = 0;
        amountBombs  = 1;
        bombRange    = 1;
    }

    int bombsOnField, amountBombs, bombRange;
};

Armoury::Armoury() :
    d(new ArmouryPrivate)
{

}

Armoury::~Armoury() { delete d; }

void Armoury::increaseBombStock() { d->amountBombs++; }

void Armoury::increaseBombRange() { d->bombRange++;  }

int Armoury::bombRange() { return d->bombRange; }

bool Armoury::dropBomb()
{
    if( d->bombsOnField == d->amountBombs )
        return false;

    d->bombsOnField++;
    return true;
}

void Armoury::returnBombAfterExplosion() { d->bombsOnField--; }

void Armoury::initialize() { d->initialize(); }
