import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.0
import GameCoordinator 1.0

ApplicationWindow {
    id: app
    visible: true
    width: 1280 // 19:11 ratio
    height: 741
    title: qsTr("Chicle Bomber (Cheeky Twins Clone) - A tribute to the 8-Bit era.")

    SwipeView {
        id: view

        currentIndex: 0
        interactive: false

        anchors.fill: parent

        Connections {
            target:  GameCoordinator
            onGameOver: view.setCurrentIndex( 2 )
        }



        StartScreen {
            id: startScreen
            onGameStart: {
                view.currentIndex = 1
                GameCoordinator.start()
            }
        }

        Battlefield {
            id: battlefield
            width:  app.width
            height: app.height
        }

        EndScreen {
            id: endScreen
            onNewGame: view.setCurrentIndex( 0 )
        }
    }
}
