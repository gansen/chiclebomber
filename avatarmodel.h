#ifndef AVATARMODEL_H
#define AVATARMODEL_H

#include <QAbstractListModel>


class AvatarModel : public QAbstractListModel
{
    Q_OBJECT
public:
    AvatarModel( QObject *parent = 0 );
    ~AvatarModel();

    enum Roles {
        pos_x = Qt::UserRole,
        pos_y,
        gameImage,
        mirrorImage,
        isAlive,
        remainingLifes,
        scoreBoardImage,
    };

    Q_INVOKABLE int numberPlayers();

    virtual int rowCount(const QModelIndex &) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual QHash<int, QByteArray> roleNames() const;


public slots:
    void gameStarted();
    void newFrame();

private:
    class AvatarModelPrivate;
    AvatarModelPrivate * d;
};

#endif // AVATARMODEL_H
