import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1

Item {
    id: base

    signal clicked
    property alias show: goButton.visible

    Text {
        id: goButton
        anchors.centerIn: parent
        text: "Press for boom!"
        font.pixelSize: 40

        Rectangle {
            id: goButtonBackground
            z: parent.z - 1
            color: "aquamarine"
            anchors.fill: parent
            radius: 20
        }

        transform: Rotation {
            axis.x: 10
            angle: 20
            SequentialAnimation on angle {
                loops: Animation.Infinite
                NumberAnimation { from: -20; to:  20;   duration: 500; easing.type: Easing.InOutQuad }
                NumberAnimation { from:  20; to: -20.5; duration: 500; easing.type: Easing.InOutQuad }
                PauseAnimation { duration: 500 }
            }
        }

        SequentialAnimation on scale {
            loops: Animation.Infinite
            NumberAnimation { from: 0.5; to: 1.5; duration: 500; easing.type: Easing.InOutQuad }
            NumberAnimation { from: 1.5; to: 0.5; duration: 500; easing.type: Easing.InOutQuad }
            PauseAnimation { duration: 250 }
        }

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onEntered: goButtonBackground.color = "red"
            onExited:  goButtonBackground.color = "aquamarine"

            onClicked: base.clicked()
        }
    }
}
