import QtQuick 2.7
import QtQuick.Layouts 1.3
import PlayerInputConfig 1.0

Item {
    id: base

    signal mouseHoveringStart
    signal mouseHoveringEnd

    property int configSelected : -1
    property PlayerInputConfig pic

    function itemHoveringStart( item, index ) {
        item.scale = 1.2
        configSelected = index
        mouseHoveringStart()
    }

    function itemHoveringEnd( item ) {
        item.scale = 1.0
        configSelected = -1
        mouseHoveringEnd()
    }

    Component {
        id: movementConfig
        Item {
            id: item
            Layout.fillWidth:  true
            Layout.fillHeight: true
            Layout.row:    index % 2
            Layout.column: index / 2

            Image {
                id: imageArrow
                width:  parent.width  / 2.0
                height: parent.height / 2.0


                anchors.horizontalCenter: parent.horizontalCenter
                source: "images/directionArrow.png"

                // Rotate the direction arrow icon CCW
                transform: Rotation {
                    origin.x: imageArrow.width  / 2.0
                    origin.y: imageArrow.height / 2.0
                    axis.z: 1
                    angle: index * -90
                }

                opacity: configSelected == index ? 1.0 : 0.7
            }

            Text {
                anchors.top: imageArrow.bottom
                anchors.horizontalCenter: imageArrow.horizontalCenter
                anchors.topMargin: 1
                opacity: configSelected == index ? 1.0 : 0.7
                text: pic.getKeycode( index )
                font.pixelSize: 6
            }

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true

                onEntered: itemHoveringStart( item, index )

                onExited:  itemHoveringEnd( item )
            }
        }
    }

    GridLayout {
        id: grid

        rows: 2;   columns: 4

        anchors.fill: parent

        Repeater {
            id: directionInput

            // The avatar can move in 4 directions
            model: 4

            delegate: movementConfig
        }

        Item {
            id: bombInput

            Layout.fillHeight: true
            Layout.fillWidth:  true
            Layout.column: 2
            Layout.row:    0
            Layout.rowSpan:    2
            Layout.columnSpan: 2

            Item {
                anchors.centerIn: parent
                width: parent.width
                height: width + textBomb.height

                opacity: configSelected == 5 ? 1.0 : 0.7

                Image {
                    id: imageBomb
                    width:  parent.width
                    height: parent.width
                    source: "images/bomb.png"
                }

                Text {
                    id: textBomb
                    anchors.top: imageBomb.bottom
                    anchors.topMargin: 4
                    anchors.horizontalCenter: imageBomb.horizontalCenter
                    text: pic.getKeycode( PlayerInputConfig.BOMB )
                    font.pixelSize: 6
                }
            }

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true

                onEntered: itemHoveringStart( bombInput, 5 )

                onExited: itemHoveringEnd( bombInput )
            }
        }
    }
}
