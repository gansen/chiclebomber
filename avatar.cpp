#include "avatar.h"

#include "battlefieldhelper.h"
#include "playerinputhelper.h"
#include "playerinputconfig.h"
#include "genericmacros.h"
#include "armoury.h"
#include "tile.h"

#include <QDebug>
#include <QTimer>

const int rebirthDelay = 1000;

class Avatar::AvatarPrivate
{
public:
    AvatarPrivate( Avatar * avatar) :
            q( avatar )
    {
        rebirthTimer.setSingleShot( true );
        connect( & rebirthTimer, SIGNAL(timeout()), avatar, SLOT(appear()) );
    }

    void loseLife() {
        isAlive = false;

        lifes--;

        if( lifes > 0 )
            rebirthTimer.start( rebirthDelay );
    }

    void updateCenterTile() {
        centerTile = BattlefieldHelper::getCenteredTile( positionX, positionY );
        centerTile->getPickup( q );
    }

    void initialize() {

        armoury.initialize();

        BattlefieldHelper::initializeStartPosition( q );

        lifes   = 5;
        isAlive = true;
    }


    ViewingDirection viewingDirection;
    Tile * centerTile; // Avatar's center lies in this tile
    PlayerInputConfig pic;
    QString sideImage, frontImage, backImage;
    Armoury armoury;
    bool isLocal;
    int  id;
    QTimer rebirthTimer;
    Avatar * q;

    // to be initialized
    int  lifes;
    bool isAlive;
    int positionX = 0, positionY = 0;
};

Avatar::Avatar() :
        d( new AvatarPrivate( this ) )
{}

Avatar::~Avatar() { delete d; }

// If the game is run in debug mode and the window is resized
// wildly, then the program crashes here, for unknown reason
int Avatar::positionX() { return d->positionX; }
int Avatar::positionY() { return d->positionY; }

void Avatar::setPositionX(int positionX)
{
    d->positionX = positionX;
    d->updateCenterTile();
}

void Avatar::setPositionY(int positionY)
{
    d->positionY = positionY;
    d->updateCenterTile();
}

SGETTER( Avatar, setSideImage,  QString, sideImage )
SGETTER( Avatar, setFrontImage, QString, frontImage )
SGETTER( Avatar, setBackImage,  QString, backImage )

SGETTER( Avatar, setIsLocal, bool, isLocal )
SGETTER( Avatar, setId,      int,  id )

Tile * Avatar::tile() { return d->centerTile; }

SGETTER_PROPERTY( Avatar, setViewingDirection, ViewingDirection, viewingDirection )


PlayerInputConfig &Avatar::playerInputConfig() { return d->pic; }

Armoury &Avatar::armoury() { return d->armoury; }

void Avatar::initialize() { d->initialize(); }

void Avatar::appear()
{
    BattlefieldHelper::initializeStartPosition( this );
    d->isAlive = true;
}


void Avatar::loseLife() {
    if( not isAlive() )
        return;

    d->loseLife();

    if( lifes() == 0 )
        emit outOfLifes( this );
}

int Avatar::lifes() { return d->lifes; }

bool Avatar::isAlive() { return d->isAlive; }
