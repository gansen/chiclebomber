import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import "./custom"
import GameServer 1.0
import GameClient 1.0

Rectangle {
    id: base

    property bool serverMode
    property bool running : switchOnOff.checked

    property string port
    property string ipAddress

    property bool readyToStartServer

    border.color: "blue"
    border.width: 1
    radius: 10

    Component.onCompleted: setSwitchLabels()

    onServerModeChanged: setSwitchLabels()

    function setSwitchLabels() {
        if( serverMode ) {
            labelOn.text  = "Start server"
            labelOff.text = "Stop server"
        } else {
            labelOn.text  = "Connect to server"
            labelOff.text = "Drop connection"
        }
    }

    Label {
        id: labelOn
        anchors.bottom: switchOnOff.top
        anchors.horizontalCenter: switchOnOff.horizontalCenter
        anchors.bottomMargin: 50
    }

    Switch {
        id: switchOnOff
        anchors.centerIn: parent

        function startServer() {
            console.log("start server")
            if( ! readyToStartServer ) {
                switchOnOff.toggle()
                return
            }
            GameServer.start( port )
        }

        function stopServer() {
            console.log("stop server")
            GameServer.stop()
        }

        function startClient() {
            console.log("start client")
            var ok = GameClient.connect( ipAddress, port )
            if( ! ok )
                switchOnOff.toggle()
        }

        function stopClient() {
            console.log("stop client")
            GameClient.disconnect()
        }

        rotation: -90
        scale: 2
        onClicked: {
            if( checked ) {
                if( serverMode ) startServer();
                else startClient();
            }
            else {
                if( serverMode ) stopServer();
                else stopClient();
            }
        }
    }

    Label {
        id: labelOff
        anchors.top: switchOnOff.bottom
        anchors.horizontalCenter: switchOnOff.horizontalCenter
        anchors.topMargin: 50
    }
}
