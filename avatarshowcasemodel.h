#ifndef AVATARSHOWCASEMODEL_H
#define AVATARSHOWCASEMODEL_H

#include <QAbstractListModel>

class AvatarShowCaseModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int playerNumber READ playerNumber WRITE setPlayerNumber NOTIFY playerNumberChanged)
public:
    explicit AvatarShowCaseModel(QObject *parent = 0);
    ~AvatarShowCaseModel();

    enum Roles {
        imagePath = Qt::UserRole,
        localPlayer,
        avatarId,
        inputConfig,
    };

    Q_INVOKABLE void createAvatar();
    Q_INVOKABLE void removeAvatar( int id);

    int  playerNumber();
    void setPlayerNumber( int n );

    // QAbstractItemModel interface
    virtual int rowCount(const QModelIndex & index = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role) const;
    virtual QHash<int, QByteArray> roleNames() const;

signals:
    void playerNumberChanged();

public slots:

    void avatarsUpdated();
private:
    class AvatarShowCaseModelPrivate;
    AvatarShowCaseModelPrivate * d;
};

#endif // AVATARSHOWCASEMODEL_H
