#ifndef PLAYERINPUTCONFIG_H
#define PLAYERINPUTCONFIG_H

#include "types.h"

#include <QObject>
#include <QHash>


class PlayerInputConfig : public QObject
{
    Q_OBJECT

public:

    // keep order for qml
    enum InputAction
    {
        UP   = 0,
        LEFT,
        DOWN,
        RIGHT,
        BOMB
    };

    PlayerInputConfig( QObject * parent = 0 );
    ~PlayerInputConfig();

    Q_ENUM(InputAction)

    bool isKeyboard();

    QHash< KeyCode, InputAction > keyConfig;

    Q_INVOKABLE QString getKeycode( InputAction action );

private:
    class PlayerInputConfigPrivate;
    PlayerInputConfigPrivate * d;
};

Q_DECLARE_METATYPE(PlayerInputConfig *)
#endif // PLAYERINPUTCONFIG_H
