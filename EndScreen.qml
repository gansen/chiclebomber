import QtQuick 2.7
import AvatarShowCaseModel 1.0
import "./custom"

Rectangle {
    id: base
    color: "linen"

    signal   newGame

    property int columns : 19
    property int rows    : 11
    property double elementWidth  : base.width  / columns
    property double elementHeight : base.height / rows

    Image {
        anchors.fill: parent
        source: "images/podest.png"
    }


    Repeater {
        id: repeater
        anchors.fill: parent

        model: AvatarShowCaseModel {
            id: avatarModel
        }

        // Entry: x, y, scale
        property var positions: [
            // First place
            [ (base.width - elementWidth) / 2.0,     base.height / 2.2,      2.0 ],
            // Second place
            [ (base.width - elementWidth) / 4.0,     base.height / 1.7,      1.5 ],
            // Third place
            [ (base.width - elementWidth) / 1.3,     base.height / 1.7,      0.7 ],
            // empty dummy, 4th place not show yet (he is going to wipe the floor)
            [ 0, 0, 0 ] ]

        delegate: Item {
            width:  elementWidth
            height: elementHeight

            x:     repeater.positions[ index ][0]
            y:     repeater.positions[ index ][1]
            scale: repeater.positions[ index ][ 2 ]

            visible:  index < 3
            Image {
                anchors.fill: parent
                source: imagePath
            }
        }
    }

    Button {
        text: "Exit game"
        anchors.top:  parent.top
        anchors.left: parent.left
        anchors.margins: 40

        onClicked: Qt.quit()
    }

    Button {
        text: "Next round"
        anchors.top:   parent.top
        anchors.right: parent.right
        anchors.margins: 40

        onClicked: newGame()
    }
}
