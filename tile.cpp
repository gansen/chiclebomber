#include "tile.h"

#include "avatar.h"
#include "armoury.h"
#include "battlefield.h"

#include <QString>

class Tile::TilePrivate
{
public:

    TilePrivate( Tile * tile ) :
        q( tile )
    {  }

    void setType( Tile::Type type ) {
        this->type = type;
        BF_instance.tileRefreshed( q->linearCoordinate );
    }

    void explosionStart() {
        setType( Tile::explosion );
    }

    void explosionStop() {
        setType( postExplosion );
        postExplosion = Tile::gangway;
        BF_instance.tileRefreshed( q->linearCoordinate );
    }

    void getPickup( Avatar * avatar ) {
        switch( type ) {

        case Tile::pickupRangeExtension:
            setType( Tile::gangway );
            avatar->armoury().increaseBombRange();
            break;

        case Tile::pickupBomb:
            setType( Tile::gangway );
            avatar->armoury().increaseBombStock();
            break;

        default:
            return;
        }
    }

    Tile::Type type;
    Tile::Type postExplosion;
    Tile * q;
};


Tile::Tile() :
    d(new TilePrivate(this))
{
    d->type          = gangway;
    d->postExplosion = gangway;
}

Tile::Tile(const Tile &t) {
    this->d = new TilePrivate(this);
         *( this->d ) = *t.d;
}

Tile &Tile::operator=(const Tile &t) {
    if( &t != this )
        *t.d = *( this->d );
    return *this;
}

Tile::~Tile() { delete d; }

bool Tile::isObstacle()
{
    switch( d->type ) {
    //obstacles
    case wall:
    case stone:
    case bomb:
        return true;
    case pickupRangeExtension:
    case gangway:
    default: //prevent warning
        return false;
    }
}

void Tile::explosionStart() { d->explosionStart(); }
void Tile::explosionStop()  { d->explosionStop();  }

void Tile::setPostExplosionType(Tile::Type type) { d->postExplosion = type; }

void Tile::getPickup(Avatar *avatar) { d->getPickup( avatar ); }

Tile::Type Tile::type() { return d->type; }

void Tile::setType(Tile::Type type) { d->setType( type ); }
