#include "gameserver.h"
#include "gamecoordinator.h"
#include "avatar.h"

#include <QTcpSocket>
#include <QTcpServer>
#include <QHash>

class GameServer::GameServerPrivate
{
public:
    GameServerPrivate() { }

    void registerConnection( QTcpSocket * socket ) {

        Avatar * avatar = GC_instance->registerPlayer( false );
        connections.insert( socket, avatar );
    }

    void unRegisterConnection( QTcpSocket * ) {
        // Avatar * avatar = connections.value( socket );
        // TODO unregister GC_instance->unregisterPlayer( avatar );
    }

    QTcpServer server;
    QHash< QTcpSocket *, Avatar * > connections;
};

GameServer::GameServer(QObject *parent) : QObject(parent)
      , d(new GameServerPrivate)
{

}

GameServer::~GameServer() { delete d; }

void GameServer::start(quint16 port)
{
    qDebug() << "start server: " << port;
    d->server.listen( QHostAddress::AnyIPv4, port );
}

void GameServer::stop() { d->server.close(); }

void GameServer::newIncommingConnection()
{
    if( not d->server.hasPendingConnections() )
        return;

    QTcpSocket * socket = d->server.nextPendingConnection();

    connect( socket, SIGNAL(disconnected()), this, SLOT(connectionDisconnected()) );

    d->registerConnection( socket );
}

void GameServer::connectionDisconnected()
{
    QTcpSocket * socket = dynamic_cast<QTcpSocket *>( QObject::sender() );
    if( not socket )
        return;

    d->unRegisterConnection( socket );
}
