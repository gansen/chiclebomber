#ifndef ARMOURY_H
#define ARMOURY_H

#include "types.h"


class Armoury
{
public:
    Armoury();
    ~Armoury();

    void increaseBombStock();
    void increaseBombRange();

    int  bombRange();

    bool dropBomb();
    void returnBombAfterExplosion();

    void initialize();

private:
    Q_DISABLE_COPY(Armoury)
    class ArmouryPrivate;
    ArmouryPrivate * d;
};

#endif // ARMOURY_H
