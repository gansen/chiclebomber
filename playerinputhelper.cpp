#include "playerinputhelper.h"

#include "playerinputconfig.h"
#include "battlefieldhelper.h"
#include "avatar.h"

const int speed = 32;

namespace PlayerInputHelper
{

void configureStandardInput(Avatar *avatar)
{
    PlayerInputConfig & pic = avatar->playerInputConfig();
    pic.keyConfig.clear();

    switch( avatar->id() ) {
    case 0:
        pic.keyConfig.insert( Qt::Key_Up,      PlayerInputConfig::UP    );
        pic.keyConfig.insert( Qt::Key_Down,    PlayerInputConfig::DOWN  );
        pic.keyConfig.insert( Qt::Key_Right,   PlayerInputConfig::RIGHT );
        pic.keyConfig.insert( Qt::Key_Left,    PlayerInputConfig::LEFT  );
        pic.keyConfig.insert( Qt::Key_Return,  PlayerInputConfig::BOMB  );
        break;

    case 1:
        pic.keyConfig.insert( Qt::Key_Q,     PlayerInputConfig::UP    );
        pic.keyConfig.insert( Qt::Key_A,     PlayerInputConfig::DOWN  );
        pic.keyConfig.insert( Qt::Key_V,     PlayerInputConfig::RIGHT );
        pic.keyConfig.insert( Qt::Key_C,     PlayerInputConfig::LEFT  );
        pic.keyConfig.insert( Qt::Key_Space, PlayerInputConfig::BOMB  );
        break;

    case 2:
        pic.keyConfig.insert( Qt::Key_W, PlayerInputConfig::UP    );
        pic.keyConfig.insert( Qt::Key_S, PlayerInputConfig::DOWN  );
        pic.keyConfig.insert( Qt::Key_J, PlayerInputConfig::RIGHT );
        pic.keyConfig.insert( Qt::Key_H, PlayerInputConfig::LEFT  );
        pic.keyConfig.insert( Qt::Key_I, PlayerInputConfig::BOMB  );
        break;

    case 3:
        pic.keyConfig.insert( Qt::Key_E, PlayerInputConfig::UP    );
        pic.keyConfig.insert( Qt::Key_D, PlayerInputConfig::DOWN  );
        pic.keyConfig.insert( Qt::Key_N, PlayerInputConfig::RIGHT );
        pic.keyConfig.insert( Qt::Key_B, PlayerInputConfig::LEFT  );
        pic.keyConfig.insert( Qt::Key_K, PlayerInputConfig::BOMB  );
        break;
    }
}

void applyMovement(Avatar *avatar, KeyCode keyCode)
{
    PlayerInputConfig &  pic = avatar->playerInputConfig();
    PlayerInputConfig::InputAction action = pic.keyConfig.value( keyCode );

    int newX, newY;

    switch( action ) {

    case PlayerInputConfig::RIGHT:
        newX = avatar->positionX() + speed;
        BattlefieldHelper::clampToRasterXright( avatar->positionX(), avatar->positionY(), newX );
        avatar->setPositionX( newX );
        avatar->setViewingDirection( ViewingDirection::Right );
        break;

    case PlayerInputConfig::LEFT:
        newX = avatar->positionX() - speed;
        BattlefieldHelper::clampToRasterXleft( avatar->positionX(), avatar->positionY(), newX );
        avatar->setPositionX( newX );
        avatar->setViewingDirection( ViewingDirection::Left );
        break;

    case PlayerInputConfig::UP:
        newY = avatar->positionY() - speed;
        BattlefieldHelper::clampToRasterYup( avatar->positionX(), avatar->positionY(), newY );
        avatar->setViewingDirection( ViewingDirection::Up );
        avatar->setPositionY( newY );
        break;

    case PlayerInputConfig::DOWN:
        newY = avatar->positionY() + speed;
        BattlefieldHelper::clampToRasterYdown( avatar->positionX(), avatar->positionY(), newY );
        avatar->setViewingDirection( ViewingDirection::Down );
        avatar->setPositionY( newY );
        break;

    case PlayerInputConfig::BOMB:
        BattlefieldHelper::placeBomb( avatar );
        break;
    }
}

}
