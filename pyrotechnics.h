#ifndef PYROTECHNICS_H
#define PYROTECHNICS_H

#include "types.h"

#include <QObject>

class Armoury;
class Tile;

#define PY_instance Pyrotechnics::instance()

class Pyrotechnics : public QObject
{
    Q_OBJECT
public:

    static Pyrotechnics & instance() {
        static Pyrotechnics pyro;
        return pyro;
    }

    bool requestExplosion( Armoury & armoury, Tile * t );

signals:

public slots:

    void boom();
    void newFrame();

private:
    explicit Pyrotechnics(QObject *parent = 0);
    ~Pyrotechnics();
    Q_DISABLE_COPY(Pyrotechnics)

    class PyrotechnicsPrivate;
    PyrotechnicsPrivate * d;
};

#endif // PYROTECHNICS_H
