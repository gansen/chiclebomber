#include "battlefield.h"

#include "battlefieldhelper.h"
#include "gamecoordinator.h"
#include "genericmacros.h"
#include "tile.h"


#include <QSet>

class Battlefield::BattlefieldPrivate
{
public:

    void initialize() {
        BattlefieldHelper::initialize();
    }

    uint columns = 0, rows = 0;

    QVector<Tile> tiles;
    QSet< LinearCoordinate > tilesToRefresh;
};


Battlefield::Battlefield(QObject *parent) : QObject(parent)
      , d( new BattlefieldPrivate )
{
    connect( GC_instance, SIGNAL(newFrame()), SLOT(newFrame()) );
}

Battlefield::~Battlefield() { delete d; }

void Battlefield::setColumns(uint columns) { d->columns = columns; }
void Battlefield::setRows(uint rows)       { d->rows    = rows; }

void Battlefield::initialize() {
    d->initialize();
    emit rasterInitialized();
}


GETTER( Battlefield, uint, columns)
GETTER( Battlefield, uint, rows)


uint Battlefield::rasterSize() { return d->tiles.size(); }

Tile * Battlefield::getTile(LinearCoordinate lc) { return & d->tiles[ lc ]; }

QVector<Tile> &Battlefield::getField() { return d->tiles; }

int Battlefield::internalTileDimension() const  { return 128; }

void Battlefield::tileRefreshed(int index) { d->tilesToRefresh.insert( index ); }

void Battlefield::newFrame()
{
    if( d->tilesToRefresh.isEmpty() )
            return;

    emit tilesChanged( d->tilesToRefresh );
    d->tilesToRefresh.clear();

}
