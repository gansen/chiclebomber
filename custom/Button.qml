import QtQuick 2.7
import QtQuick.Controls 2.1

Rectangle {
    id: base

    signal clicked
    property alias text  : text.text
    property bool  isActive : false

    function reset() {
        isActive = false
    }

    onIsActiveChanged: {
        if( isActive )
            base.border.width = 2
        else
            base.border.width = 0
    }

    color: "aquamarine"
    radius: 10

    width:  text.width  + 12
    height: text.height + 14

    Text {
        id: text
        anchors.centerIn: parent
        font.pixelSize: 20
    }

    MouseArea {
        anchors.fill: parent

        onClicked: {
            base.clicked()
            isActive = true
        }
    }
}
