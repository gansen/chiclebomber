import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import "./custom"

Rectangle {

    Image {
        id: imageSetting
        anchors.fill: parent
        anchors.margins: 10
        source: "images/settings_icon.png"

        transform: Rotation {
            origin.x: imageSetting.width / 2
            origin.y: imageSetting.height / 2
            axis.z: 1

            SequentialAnimation on angle {
                id: animation
                loops: Animation.Infinite
                alwaysRunToEnd: true
                running: false
                NumberAnimation { from:   0; to: 360; duration: 3000; easing.type: Easing.InOutCubic }
                NumberAnimation { from: 360; to:   0; duration: 3000; easing.type: Easing.InOutCubic }
                PauseAnimation  { duration: 500 }
            }
        }
    }

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true

        onEntered: {
            buttonSettings.scale = 1.4
            animation.start()
        }
        onExited: {
            buttonSettings.scale = 1.0
            animation.stop()
        }

        onClicked: dialog.open()
    }

    Dialog {
        id: dialog

        contentItem: Rectangle {
            Button {
                text: "Done"
                onClicked: dialog.close()
            }
        }
    }
}
