#ifndef TILE_H
#define TILE_H

#include "types.h"

class Avatar;

class Tile
{
public:
    Tile();
    Tile( const Tile & t );
    Tile & operator=( const Tile & t );
    ~Tile();

    enum Type {
        wall,
        stone,
        gangway,
        pickupRangeExtension,
        pickupBomb,
        explosion,
        bomb, // not to be picked up
    };

    bool isObstacle();

    LinearCoordinate linearCoordinate;

    Tile * left;
    Tile * right;
    Tile * up;
    Tile * down;

    void explosionStart();
    void explosionStop();

    void setPostExplosionType( Tile::Type type );

    void getPickup( Avatar * avatar );

    Type type();
    void setType( Type type );

private:
    class TilePrivate;
    TilePrivate * d;
};

#endif // TILE_H
