#include "avatarmodel.h"

#include "gamecoordinator.h"
#include "battlefield.h"
#include "avatar.h"

#include "types.h"

class AvatarModel::AvatarModelPrivate
{
public:
    AvatarModelPrivate( AvatarModel * host ) {
        q = host;
    }

    void refreshModel() {
        q->beginResetModel();
        q->endResetModel();
    }

    AvatarModel * q;
};

AvatarModel::AvatarModel(QObject *parent) : QAbstractListModel(parent)
      , d(new AvatarModelPrivate(this))
{
    connect( GC_instance, SIGNAL(gameStarted()), this, SLOT(gameStarted()) );
    connect( GC_instance, SIGNAL(newFrame()),    this, SLOT(newFrame()) );
}

AvatarModel::~AvatarModel() { delete d; }

int AvatarModel::numberPlayers() { return GC_instance->numberPlayers(); }


int AvatarModel::rowCount(const QModelIndex &) const
{
    return GC_instance->avatars().size();
}

QVariant AvatarModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    Avatar * avatar = GC_instance->avatars()[ row ];

    switch( role ) {
    case Roles::pos_x:
        return QVariant( avatar->positionX() * 1.0 / BF_instance.internalTileDimension() );
    case Roles::pos_y:
        return QVariant( avatar->positionY() * 1.0 / BF_instance.internalTileDimension() );
    case Roles::gameImage:
        if( avatar->viewingDirection() == ViewingDirection::Left
         || avatar->viewingDirection() == ViewingDirection::Right )
        return QVariant( avatar->sideImage() );
        else
        if( avatar->viewingDirection() == ViewingDirection::Down )
        return QVariant( avatar->frontImage() );
        else
        return QVariant( avatar->backImage() );
    case Roles::mirrorImage:
        return QVariant( avatar->viewingDirection() == ViewingDirection::Right );
    case Roles::isAlive:
        return QVariant( avatar->isAlive() );
    case Roles::remainingLifes:
        return QVariant( avatar->lifes() );
    case Roles::scoreBoardImage:
        return QVariant( avatar->frontImage() );
    }

    return QVariant();
}

QHash<int, QByteArray> AvatarModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[ Roles::pos_x ]           = "pos_x";
    roles[ Roles::pos_y ]           = "pos_y";
    roles[ Roles::gameImage ]       = "gameImage";
    roles[ Roles::mirrorImage ]     = "mirrorImage";
    roles[ Roles::isAlive ]         = "isAlive";
    roles[ Roles::remainingLifes ]  = "remainingLifes";
    roles[ Roles::scoreBoardImage ] = "scoreBoardImage";

    return roles;
}

void AvatarModel::gameStarted() { d->refreshModel(); }

void AvatarModel::newFrame()    { d->refreshModel(); }
