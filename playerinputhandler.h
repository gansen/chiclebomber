#ifndef PLAYERINPUTHANDLER_H
#define PLAYERINPUTHANDLER_H

#include <QObject>

class PlayerInputHandler : public QObject
{
    Q_OBJECT

public:

    explicit PlayerInputHandler( QObject * parent = 0 );
    virtual ~PlayerInputHandler();

    // QObject interface
    bool eventFilter(QObject *obj, QEvent *event);

    void start();
    void stop();

signals:

public slots:

private slots:
    void keyboardSample();

private:
    class PlayerInputHandlerPrivate;
    PlayerInputHandlerPrivate * d;
};



#endif // PLAYERINPUTHANDLER_H
