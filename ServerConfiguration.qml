import QtQuick 2.0
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import "./custom"

ColumnLayout {
    id: base

    signal clicked
    property alias port : port.text
    property bool isActive

    // alias does not work for unknown reason
    onIsActiveChanged: { buttonBeServer.isActive = isActive }

    height: 80
    width:  buttonBeServer.width

    Item {
        anchors.fill: parent

        Button {
            id: buttonBeServer
            text: "Start as Server"

            onClicked:  base.clicked()
        }

        RowLayout {
            visible: isServerMode
            anchors.top: buttonBeServer.bottom
            anchors.horizontalCenter: buttonBeServer.horizontalCenter
            anchors.margins: 10

            Label {
                id: labelServerPort
                text: "Port:"
                font.pixelSize: 16
            }

            TextInput {
                id: port
                width: 200
                text: "64000"
                font.pixelSize: 20
            }
        }
    }
}
