#ifndef AVATAR_H
#define AVATAR_H

#include "types.h"

#include <QObject>
#include <QString>

class PlayerInputConfig;
class Armoury;
class Tile;

class Avatar : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int positionX READ positionX WRITE setPositionX )
    Q_PROPERTY(int positionY READ positionY WRITE setPositionY )
    Q_PROPERTY(int lifes     READ lifes )

    Q_PROPERTY(QString sideImage  READ sideImage  WRITE setSideImage  )
    Q_PROPERTY(QString frontImage READ frontImage WRITE setFrontImage )
    Q_PROPERTY(QString backImage  READ backImage  WRITE setBackImage )
    Q_PROPERTY(ViewingDirection viewingDirection READ viewingDirection WRITE setViewingDirection NOTIFY viewingDirectionChanged)

public:
    Avatar();
    ~Avatar();

    Q_ENUM( ViewingDirection )

    ViewingDirection viewingDirection();
    void setViewingDirection( ViewingDirection viewingDirection );

    // Position
    int  positionX();
    int  positionY();
    void setPositionX( int positionX );
    void setPositionY( int positionY );
    Tile * tile();

    QString sideImage();
    void    setSideImage( QString sideImage );

    QString frontImage();
    void    setFrontImage( QString frontImage );

    QString backImage();
    void    setBackImage( QString backImage );

    bool isLocal();
    void setIsLocal( bool isLocal );
    void setId( int id );
    int  id();

    void loseLife();
    int  lifes();
    bool isAlive();

    PlayerInputConfig & playerInputConfig();

    Armoury & armoury();

    void initialize();

signals:
    void viewingDirectionChanged();
    void outOfLifes( Avatar * avatar );

public slots:
    void appear();

private:
    Q_DISABLE_COPY(Avatar)
    class AvatarPrivate;
    AvatarPrivate * d;
};

#endif // AVATAR_H
