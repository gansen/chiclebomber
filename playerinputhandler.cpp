#include "playerinputhandler.h"

#include "playerinputhelper.h"
#include "playerinputconfig.h"
#include "gamecoordinator.h"
#include "avatar.h"

#include <QGuiApplication>
#include <QKeyEvent>
#include <QVector>
#include <QTimer>
#include <QDebug>
#include <QHash>

class PlayerInputHandler::PlayerInputHandlerPrivate
{
public:
    PlayerInputHandlerPrivate( PlayerInputHandler * pih ) :
            pih(pih)
    {
        // A controlled short key stroke takes about 30 ms.
        // But since this it is more likely that a keystroke takes 50 ms or more
        // we settle for a 30 ms sampling rate. While this is not sufficient for
        // Shannon, it might be ok for the game.
        connect( &keyboardTimer, SIGNAL(timeout()), pih, SLOT(keyboardSample()) );
        keyboardTimer.setInterval( 30 );

        // Reserve space for 10 key strokes.
        currentKeysPressed.reserve( 10 );
    }

    void reset() {
        playersUsingKeyboard.clear();
    }

    void start() {

        reset();

        // Get local players using keyboard as input device
        for( Avatar * a : GC_instance->avatars() ) {
            if( a->isLocal() && a->playerInputConfig().isKeyboard() )
                playersUsingKeyboard << a;
        }

        QGuiApplication::instance()->installEventFilter( pih );
        keyboardTimer.start();
    }

    void stop() {
        keyboardTimer.stop();
        QGuiApplication::instance()->removeEventFilter( pih );
    }

    void registerKeyRelease( QKeyEvent * keyEvent ) {

        if( keyEvent->isAutoRepeat() ) return;

        currentKeysPressed.take( keyEvent->key() );
    }

    void registerKeyPress( QKeyEvent * keyEvent ) {

        if( keyEvent->isAutoRepeat() ) return;

        for( Avatar * avatar : GC_instance->avatars() ) {
            if( avatar->playerInputConfig().keyConfig.contains( keyEvent->key() ) ) {
                currentKeysPressed.insert( keyEvent->key(), avatar );
                break;
            }
        }
    }

    QVector<Avatar *>    playersUsingKeyboard;
    PlayerInputHandler * pih;
    QHash< KeyCode, Avatar * > currentKeysPressed;
    QTimer keyboardTimer;
};

PlayerInputHandler::PlayerInputHandler(QObject *parent) : QObject(parent)
      , d(new PlayerInputHandlerPrivate(this))
{
}

PlayerInputHandler::~PlayerInputHandler() { delete d; }

bool PlayerInputHandler::eventFilter(QObject *obj, QEvent *event) {

    // maybe check for autorepeat
    switch( event->type() ) {
    case QEvent::KeyPress:
        d->registerKeyPress( static_cast<QKeyEvent *>(event) );
        return true;

    case QEvent::KeyRelease:
        d->registerKeyRelease( static_cast<QKeyEvent *>(event) );
        return true;

    default:
        return QObject::eventFilter(obj, event);
    }
}


void PlayerInputHandler::start() { d->start(); }
void PlayerInputHandler::stop()  { d->stop();  }

void PlayerInputHandler::keyboardSample()
{
    QHash< KeyCode, Avatar * >::const_iterator i = d->currentKeysPressed.constBegin();

    while( i != d->currentKeysPressed.constEnd() ) {
        Avatar * avatar = i.value();
        KeyCode keyCode = i.key();

        PlayerInputHelper::applyMovement( avatar, keyCode );
        ++i;
    }
}
