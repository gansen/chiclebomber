#include "playerinputconfig.h"

#include <QKeySequence>

class PlayerInputConfig::PlayerInputConfigPrivate
{
public:
};

PlayerInputConfig::PlayerInputConfig( QObject * parent ) : QObject( parent )
  , d(new PlayerInputConfigPrivate)
{ }

PlayerInputConfig::~PlayerInputConfig() { delete d; }

bool PlayerInputConfig::isKeyboard() { return true; }

QString PlayerInputConfig::getKeycode( InputAction action )
{
    QKeySequence seq( keyConfig.keys( action ).first() );
    return seq.toString();
}
