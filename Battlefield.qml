import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import BattlefieldModel 1.0
import GameCoordinator 1.0
import AvatarModel 1.0
import Avatar 1.0

Rectangle {
    id: base
    color: "lightgoldenrodyellow"
    // or? color: "#ffff40"

    property int columns : 19
    property int rows    : 11
    property double elementWidth  : battlefield.width  / columns
    property double elementHeight : battlefield.height / rows

    Item {
        id: battlefield
        anchors { top: parent.top; bottom: scoreBoard.top }
        width: parent.width

        Item {
            id: main;
            anchors.fill: parent

            GridView {
                id: grid;
                anchors.fill: parent
                cellWidth:  elementWidth
                cellHeight: elementHeight
                interactive: false

                model: BattlefieldModel {
                    id: bfModel
                    columns: base.columns; rows: base.rows
                    wallImage:  "/images/wall.png"
                    stoneImage: "/images/stone.png"
                    bombImage:  "/images/bomb.png"
                    explosionImage: "/images/explosion.png"
                    rangeExtension: "/images/rangeExtension.png"
                    pickupBomb: "/images/pickupBomb.gif"
                }

                delegate: Item {
                    width: grid.cellWidth ; height: grid.cellHeight ;

                    Image {
                        id: image
                        anchors.fill: parent
                        source: content
                    }
                }
            }

            GridView {
                id: animatedPickupGrid
                anchors.fill: parent
                cellWidth:  elementWidth
                cellHeight: elementHeight
                interactive: false

                model: bfModel

                delegate:  Item {
                    width: grid.cellWidth ; height: grid.cellHeight ;

                    AnimatedImage {
                        id: animatedImage
                        anchors.fill: parent
                        source: animated
                        scale: 0.8
                    }
                }
            }
        }

        Repeater {
            anchors.fill: parent
            model: AvatarModel { }
            delegate: Rectangle {
                color: "transparent"
                width:  elementWidth; height: elementHeight
                x: pos_x * elementWidth
                y: pos_y * elementHeight
                visible: isAlive
                Image {
                    anchors.fill: parent
                    source: gameImage
                    mirror: mirrorImage
                }
            }
        }
    }

    Rectangle {
        id: scoreBoard
        anchors.bottom: parent.bottom
        width: parent.width
        height: 90
        color: "transparent"


        Row {
            id: lifeBoard
            anchors.fill: parent
            anchors.margins: 5

            Repeater {
                anchors.fill: parent

                model: AvatarModel { id: modelAvatar }

                delegate: Rectangle {
                    color: "transparent"
                    width:  base.width / modelAvatar.numberPlayers()
                    height: parent.height
                    Image {
                        id: scoreAvatar
                        width:  parent.height * 0.55
                        height: parent.height * 0.55
                        anchors { verticalCenter: parent.verticalCenter }
                        source: scoreBoardImage
                        mirror: true
                    }

                    Text {
                        color: "#15e245"
                        anchors.left: scoreAvatar.right
                        anchors.verticalCenter: scoreAvatar.verticalCenter
                        anchors.leftMargin: 20
                        text: "x" + remainingLifes
                        font.family: "Purisa"
                        styleColor: "blue"
                        style: Text.Outline
                        font.pixelSize: 50
                        font.weight:  Font.ExtraBold
                        font.letterSpacing: 12
                    }
                }
            }
        }
    }
}
