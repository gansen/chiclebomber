#include "gameclient.h"
#include <QTcpSocket>
#include <QHostAddress>
#include <QDebug>
#include "genericmacros.h"

class GameClient::GameClientPrivate
{
public:
    GameClientPrivate() :
            socket( nullptr )
          , isConnected( false )
    {}

    ~GameClientPrivate() {
        delete socket;
    }

    bool connect( QString host, quint16 port ) {
        socket = new QTcpSocket;
        QHostAddress address;
        qDebug() << "connecting to: " << address << host << port;
        if( not address.setAddress( host ) )
            return false;

        socket->connectToHost( host, port );
        return true;
    }

    QTcpSocket * socket;
    bool isConnected;
};

bool GameClient::connect(QString host, quint16 port) { return d->connect( host, port ); }

void GameClient::disconnect() {
    // Not sure if this is appropriate
    // But it works in case a connection to the same
    // host is demanded consecutively in a short periode
    // of time.
    d->socket->disconnectFromHost();
    d->socket->deleteLater();
    d->socket = nullptr;
}

GETTER_PROPERTY( GameClient, bool, isConnected )

GameClient::GameClient(QObject *parent) : QObject(parent)
      , d(new GameClientPrivate)
{ }

GameClient::~GameClient() { delete d; }
