#ifndef BATTLEFIELD_H
#define BATTLEFIELD_H

#include "types.h"

#include <QObject>
#include <QVector>

class Tile;

#define BF_instance Battlefield::instance()

class Battlefield : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Battlefield)

public:

    static Battlefield & instance() {
        static Battlefield bf;
        return bf;
    }

    void setColumns( uint columns );
    void setRows( uint rows );

    uint rows();
    uint columns();

    uint rasterSize(); // in tiles

    // no boundaries are checked
    Tile * getTile( LinearCoordinate lc );

    QVector<Tile> & getField();

    int internalTileDimension() const;

    void tileRefreshed( int index );

    void initialize();

signals:
    void tilesChanged( const QSet< LinearCoordinate > & indices );
    void rasterInitialized();

public slots:
    void newFrame();

private:
    explicit Battlefield(QObject *parent = 0);
    ~Battlefield();

    class BattlefieldPrivate;
    BattlefieldPrivate * d;
};



#endif // BATTLEFIELD_H
