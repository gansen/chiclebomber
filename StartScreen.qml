import QtQuick 2.0
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import "./custom"

Rectangle {

    signal gameStart

    property bool isServerMode : false
    property bool isClientMode : false
    property bool isLocalMode  : ! isServerMode && ! isClientMode

    ColumnLayout {
        anchors.fill: parent

        RowLayout {
            id: networkConfigLayout

            height: 300

            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter
            Layout.margins: 20

            ColumnLayout {
                id: buttonGameTypeConfig
                Layout.preferredWidth:  200
                Layout.preferredHeight: 250
                Layout.alignment: Qt.AlignCenter

                LocalConfiguration {
                    id: localConfiguration
                    enabled:  ! startSwitch.running || ! startSwitch.visible
                    isActive: ! isServerMode        && ! isClientMode

                    Layout.alignment: Qt.AlignHCenter

                    onClicked: {
                        isServerMode = false
                        isClientMode = false
                    }
                }

                ServerConfiguration {
                    id: serverConfiguration
                    enabled: ! startSwitch.running || ! startSwitch.visible
                    isActive: isServerMode

                    Layout.alignment: Qt.AlignHCenter

                    onClicked: {
                        isServerMode = true
                        isClientMode = false
                        startSwitch.port = port
                    }
                    onPortChanged: startSwitch.port = port
                }

                ClientConfiguration {
                    id: clientConfiguration
                    enabled: ! startSwitch.running || ! startSwitch.visible
                    isActive: isClientMode

                    Layout.alignment: Qt.AlignHCenter

                    onClicked: {
                        isServerMode = false
                        isClientMode = true
                        startSwitch.ipAddress = ipAddress
                        startSwitch.port      = port
                    }
                    onIpAddressChanged: startSwitch.ipAddress = ipAddress
                    onPortChanged:      startSwitch.port      = port
                }

            }

            StartNetworkSwitch {
                id: startSwitch

                Layout.preferredWidth: 130
                Layout.alignment: Qt.AlignCenter
                height: 180

                visible: isClientMode || isServerMode

                serverMode: isServerMode
                readyToStartServer: showCaseList.readyToStart
            }

            Label {
                Layout.preferredWidth: width
                Layout.alignment: Qt.AlignCenter

                visible: isClientMode || isServerMode

                text: "Networking is\nnot yet implemented."
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 30
                color: "red"
            }
        }

        ButtonSettings  {
            id: buttonSettings
            Layout.preferredHeight: 50
            Layout.preferredWidth:  50
            Layout.alignment: Qt.AlignHCenter

            radius: 20
            color: "lightsteelblue"
        }

        RowLayout {
            Layout.fillWidth:  true

            Layout.preferredHeight: width / 4
            Layout.maximumHeight:   width / 4

            Layout.leftMargin: 40
            Layout.rightMargin: 40

            Rectangle {
                id: showCase
                Layout.fillWidth:  true
                Layout.fillHeight: true
                Layout.alignment: Qt.AlignCenter
                color: "lightsteelblue"

                AvatarShowCase {
                    id: showCaseList
                    anchors.centerIn: parent
                    height: parent.height - 5
                    width:  parent.width - 20
                }
            }
        }


        ButtonStartGame  {
            Layout.alignment: Qt.AlignHCenter
            Layout.preferredHeight: 200
            Layout.fillWidth: true
            Layout.fillHeight: true
            show: startSwitch.running && isServerMode || isLocalMode && showCaseList.readyToStart

            onClicked: gameStart()
        }
    }
}
